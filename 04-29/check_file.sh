#!/bin/bash

FILE=$1
if [[ ! -f "$FILE" ]];then
    echo "El archivo $FILE no existe"
    exit 1
fi
if [[ -x "$FILE" ]];then
    ls -lha "$FILE"
        read -p "Desea ejecutar el archivo $FILE? [S/N]" sn
        if [[ $sn = [Ss] ]];then
		echo "Ejecutando..."
		 exit 0
        elif [[ $sn = [Nn] ]];then
		 exit 0
	else echo "Respuesta invalida"
	exit 1;
        fi
elif [[ -O "$FILE" ]];then
	read -p "El archivo $FILE no tiene permisos de ejecucion. Desea otorgarle? [S/N]" sn
	if [[  $sn = [Ss] ]];then
		chmod u+x "$FILE"
		ls -lha "$FILE"
		exit 0
        elif [[ $sn = [Nn] ]];then
		 exit 0
        else echo "Respuesta invalida"
	fi
else echo "No es owner del archivo"
fi
